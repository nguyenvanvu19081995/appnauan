package com.example.workspace.nauan;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.workspace.nauan.controller.MonAnController;
import com.example.workspace.nauan.model.MonAn;
import com.github.johnpersano.supertoasts.library.Style;
import com.github.johnpersano.supertoasts.library.SuperActivityToast;
import com.github.johnpersano.supertoasts.library.utils.PaletteUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Random;

public class HomNayAnGiActivity extends AppCompatActivity {

    private CheckBox checkBoxMB, checkBoxMTrung, checkBoxMN, checkBoxMTay;
    private LinearLayout linearLayoutChon;
    private ProgressBar progressBar;
    private ImageView imgMonAn,imgDauHoi;
    private TextView txtTenMonAn;
    private MonAnController monAnController;
    private static HomNayAnGiActivity instance;
    private int position;

    public static HomNayAnGiActivity getInstance() {
        return instance;
    }

    public static Context getContext() {
        return instance.getApplicationContext();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hom_nay_an_gi);

        instance = this;

        checkBoxMB = (CheckBox) findViewById(R.id.checkBoxMB);
        checkBoxMTrung = (CheckBox) findViewById(R.id.checkBoxMTrung);
        checkBoxMN = (CheckBox) findViewById(R.id.checkBoxMN);
        checkBoxMTay = (CheckBox) findViewById(R.id.checkBoxMTay);
        linearLayoutChon = (LinearLayout) findViewById(R.id.chon);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        imgMonAn = (ImageView) findViewById(R.id.imgMonAn);
        txtTenMonAn = (TextView) findViewById(R.id.txtTenMonAn);
        imgDauHoi = (ImageView) findViewById(R.id.dauhoi);
        getSupportActionBar().setTitle("Hôm nay ăn gì?");
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#8F0461")));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        linearLayoutChon.setOnClickListener(onChon_Click);
        imgMonAn.setOnClickListener(MonAn_OnClick);
        progressBar.setVisibility(View.GONE);
        txtTenMonAn.setSelected(true);
        txtTenMonAn.setSingleLine(true);

    }

    private View.OnClickListener MonAn_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            MonAn monAn = new MonAn();
            Intent chitiet = new Intent(HomNayAnGiActivity.this, ChiTietMonAnActivity.class);
            monAn = getListMonAn().get(position);
            chitiet.putExtra("MONAN",monAn);
            startActivityForResult(chitiet,50);
        }
    };

    private View.OnClickListener onChon_Click = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            final Animation myAnim = AnimationUtils.loadAnimation(v.getContext(), R.anim.anim_chon);
            linearLayoutChon.startAnimation(myAnim);
            Log.d("SoLuongMon: ",getListMonAn().size()+"");
            if(getListMonAn().size()>0) {
                imgMonAn.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                imgDauHoi.setVisibility(View.GONE);
                txtTenMonAn.setText("");
                final long duration = 500;
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();

                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                imgMonAn.setVisibility(View.VISIBLE);
                                MonAn monAn = ranDom(getListMonAn());
                                Picasso.with(v.getContext()).load(monAn.getHinhAnh()).into(imgMonAn);
                                txtTenMonAn.setText(monAn.getTenMonAn());
                                Animation animation = AnimationUtils.loadAnimation(HomNayAnGiActivity.this, R.anim.zoom_textview);
                                txtTenMonAn.setAnimation(animation);
                                // Set hieu ung zoom
//                                ZoomAnimation zoomAnimation = new ZoomAnimation(HomNayAnGiActivity.this);
//                                zoomAnimation.zoomReverse(imgMonAn, duration);

                                progressBar.setVisibility(View.GONE);
                                SuperActivityToast.create(HomNayAnGiActivity.this, new Style(), Style.TYPE_BUTTON)
                                        .setProgressBarColor(Color.WHITE)
                                        .setText("Món này được chưa? ^-^")
                                        .setDuration(Style.DURATION_LONG)
                                        .setFrame(Style.FRAME_LOLLIPOP)
                                        .setColor(PaletteUtils.getSolidColor(PaletteUtils.MATERIAL_PURPLE))
                                        .setAnimations(Style.ANIMATIONS_POP).show();
                            }
                        });
                    }
                });
                thread.start();
            }
            else if (getListMonAn().size()==0){
                SuperActivityToast.create(HomNayAnGiActivity.this, new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText("Bạn chưa chọn miền kìa! ^-^")
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(PaletteUtils.getSolidColor(PaletteUtils.MATERIAL_PURPLE))
                        .setAnimations(Style.ANIMATIONS_POP).show();
            }
        }
    };

    private MonAn ranDom(ArrayList<MonAn> listMonAn) {
        int sizeList = listMonAn.size();
        Random r = new Random();
        position = r.nextInt(sizeList);
        MonAn monAn = listMonAn.get(position);
        return monAn;
    }

    private ArrayList<MonAn> getListMonAn(){
        monAnController = new MonAnController(getContext());
        ArrayList<MonAn> listMonAn = new ArrayList<>();
        Boolean checkMB = checkBoxMB.isChecked();
        Boolean checkMN = checkBoxMN.isChecked();
        Boolean checkMTrung = checkBoxMTrung.isChecked();
        Boolean checkMTay = checkBoxMTay.isChecked();
        if(checkMB){
            for (MonAn monan:monAnController.getMonAnMienBac()) {
                listMonAn.add(monan);
            }
        }
        if(checkMN){
            for (MonAn monan:monAnController.getMonAnMienNam()) {
                listMonAn.add(monan);
            }
        }
        if(checkMTrung){
            for (MonAn monan:monAnController.getMonAnMienTrung()) {
                listMonAn.add(monan);
            }
        }
        if(checkMTay){
            for (MonAn monan:monAnController.getMonAnMienTay()) {
                listMonAn.add(monan);
            }
        }
        return listMonAn;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
