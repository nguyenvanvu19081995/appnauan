package com.example.workspace.nauan;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.workspace.nauan.model.MeoVat;
import com.squareup.picasso.Picasso;

public class ChiTietMeoVatActivity extends AppCompatActivity {
    private ImageView imgMeoVat;
    private TextView txtChiTiet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chi_tiet_meo_vat);

        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#8F0461")));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        imgMeoVat = (ImageView) findViewById(R.id.imgMeoVat);
        txtChiTiet = (TextView) findViewById(R.id.chitiet);

        MeoVat meoVat = (MeoVat) getIntent().getExtras().getSerializable("MEOVAT");
        getSupportActionBar().setTitle(meoVat.getName());
        Picasso.with(getApplicationContext()).load(meoVat.getHinhAnh()).into(imgMeoVat);
        txtChiTiet.setText(meoVat.getChiTiet());

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
