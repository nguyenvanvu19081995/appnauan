package com.example.workspace.nauan;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.workspace.nauan.adapter.PagerAdapter;
import com.example.workspace.nauan.controller.MonAnController;
import com.example.workspace.nauan.controller.SQLiteDataController;
import com.example.workspace.nauan.model.MonAn;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private ViewPager viewPager;
    private TabLayout tabLayout;
    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        //setToolbar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        SQLiteDataController sql = new SQLiteDataController(this);
        sql.isCreatedDatabase();

        viewPager = (ViewPager) findViewById(R.id.view_pager);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        FragmentManager manager = getSupportFragmentManager();
        PagerAdapter adapter = new PagerAdapter(manager);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setTabsFromPagerAdapter(adapter);


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        disableNavigationViewScrollbars(navigationView);

    }


    public void setToolbar(Toolbar toolbar) {
        if(toolbar != null) {
            setSupportActionBar(toolbar);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            toggle.syncState();
        } else {
            drawer.setDrawerListener(null);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        switch (item.getItemId()){
            case R.id.mnTip:
                Intent meoVat = new Intent(MainActivity.this,MeoVatActivity.class);
                startActivity(meoVat);

                break;
            case R.id.mnFavorite:
                Intent yeuThich = new Intent(MainActivity.this,MonAnYeuThichActivity.class);
                startActivity(yeuThich);

                break;
            case R.id.mnToday:
                Intent anGi = new Intent(MainActivity.this,HomNayAnGiActivity.class);
                startActivity(anGi);

                break;
            case R.id.mnGoodStore:

                break;
            case R.id.mnInfo:

                break;
            case R.id.mnRate:

                break;
            case R.id.mnFeed:

                break;
            case R.id.mnFacebook:

                break;
            case R.id.mnTwitter:

                break;
            case R.id.mnGoogle:

                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void disableNavigationViewScrollbars(NavigationView navigationView) {
        if (navigationView != null) {
            NavigationMenuView navigationMenuView = (NavigationMenuView) navigationView.getChildAt(0);
            if (navigationMenuView != null) {
                navigationMenuView.setVerticalScrollBarEnabled(false);
            }
        }
    }

    public ArrayList<MonAn> getMAMB(){
        return new MonAnController(getApplicationContext()).getMonAnMienBac();
    }
    public ArrayList<MonAn> getMAMN(){
        return new MonAnController(getApplicationContext()).getMonAnMienNam();
    }
    public ArrayList<MonAn> getMAMTrung(){
        return new MonAnController(getApplicationContext()).getMonAnMienTrung();
    }
    public ArrayList<MonAn> getMAMTay(){
        return new MonAnController(getApplicationContext()).getMonAnMienTay();
    }
}
