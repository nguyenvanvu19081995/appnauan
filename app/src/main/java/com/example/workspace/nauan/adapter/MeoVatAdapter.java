package com.example.workspace.nauan.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.workspace.nauan.R;
import com.example.workspace.nauan.model.MeoVat;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by WorkSpace on 11/26/2016.
 */

public class MeoVatAdapter extends BaseAdapter {
    private ArrayList<MeoVat> listData;
    private LayoutInflater inflater;

    public MeoVatAdapter(ArrayList<MeoVat> listData, LayoutInflater inflater) {
        this.listData = listData;
        this.inflater = inflater;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public MeoVat getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null)
            convertView = inflater.inflate(R.layout.item_listview,null);
        ImageView imgMonAn = (ImageView) convertView.findViewById(R.id.imgMonAn);
        TextView txtName = (TextView) convertView.findViewById(R.id.txtTenMonAn);

        Picasso.with(inflater.getContext()).load(listData.get(position).getHinhAnh()).into(imgMonAn);
        txtName.setText(listData.get(position).getName());
        txtName.setSingleLine(true);
        txtName.setSelected(true);
        return convertView;
    }
}
