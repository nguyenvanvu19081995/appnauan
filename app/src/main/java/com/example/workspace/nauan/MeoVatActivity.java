package com.example.workspace.nauan;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

import com.example.workspace.nauan.adapter.MeoVatAdapter;
import com.example.workspace.nauan.controller.MonAnController;
import com.example.workspace.nauan.model.MeoVat;
import com.twotoasters.jazzylistview.JazzyListView;

public class MeoVatActivity extends AppCompatActivity {
    private JazzyListView listView;
    private static MeoVatActivity instance;
    private MeoVatAdapter adapter;
    private MonAnController monAnController;


//    public static MeoVatActivity getInstance() {
//        return instance;
//    }

    public static Context getContext() {
        return instance.getApplicationContext();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meo_vat);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#8F0461")));
        getSupportActionBar().setTitle("Mẹo vặt");
        instance = this;
        monAnController = new MonAnController(getContext());

        listView = (JazzyListView) findViewById(R.id.list);
        adapter = new MeoVatAdapter(monAnController.getMeoVat(),getLayoutInflater());
        //AlphaAnimatorAdapter animatorAdapter = new AlphaAnimatorAdapter(adapter, listView);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(onItem_Click);
        Log.d("ListMeoVat ",monAnController.getMeoVat().size()+"");

    }

    private AdapterView.OnItemClickListener onItem_Click = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            MeoVat meoVat = adapter.getItem(i);
            Intent chiTietMeoVat = new Intent(MeoVatActivity.this,ChiTietMeoVatActivity.class);
            chiTietMeoVat.putExtra("MEOVAT",meoVat);
            startActivityForResult(chiTietMeoVat,20);
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
