package com.example.workspace.nauan.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.workspace.nauan.fragment.Fragment_MienBac;
import com.example.workspace.nauan.fragment.Fragment_MienNam;
import com.example.workspace.nauan.fragment.Fragment_MienTay;
import com.example.workspace.nauan.fragment.Fragment_MienTrung;

/**
 * Created by WorkSpace on 11/24/2016.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {
    public PagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment frag=null;
        switch (position){
            case 0:
                frag =  new Fragment_MienBac();
                break;
            case 1:
                frag =  new Fragment_MienNam();
                break;
            case 2:
                frag =  new Fragment_MienTrung();
                break;
            case 3:
                frag = new Fragment_MienTay();
                break;
        }
        return frag;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        switch (position){
            case 0:
                title="Miền Bắc";
                break;
            case 1:
                title="Miền Trung";
                break;
            case 2:
                title="Miền Nam";
                break;
            case 3:
                title="Miền Tây";
                break;
        }

        return title;
    }
}
