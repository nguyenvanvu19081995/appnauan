package com.example.workspace.nauan.model;

import java.io.Serializable;

/**
 * Created by WorkSpace on 11/26/2016.
 */

public class MeoVat implements Serializable {
    private int id;
    private String name;
    private String chiTiet;
    private String hinhAnh;

    public MeoVat(int id, String name, String chiTiet, String hinhAnh) {
        this.id = id;
        this.name = name;
        this.chiTiet = chiTiet;
        this.hinhAnh = hinhAnh;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChiTiet() {
        return chiTiet;
    }

    public void setChiTiet(String chiTiet) {
        this.chiTiet = chiTiet;
    }

    public String getHinhAnh() {
        return hinhAnh;
    }

    public void setHinhAnh(String hinhAnh) {
        this.hinhAnh = hinhAnh;
    }
}
