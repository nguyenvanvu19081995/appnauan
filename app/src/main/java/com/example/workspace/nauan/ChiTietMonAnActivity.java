package com.example.workspace.nauan;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.workspace.nauan.controller.MonAnController;
import com.example.workspace.nauan.model.MonAn;
import com.github.johnpersano.supertoasts.library.Style;
import com.github.johnpersano.supertoasts.library.SuperActivityToast;
import com.github.johnpersano.supertoasts.library.utils.PaletteUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Random;

public class ChiTietMonAnActivity extends AppCompatActivity {

    private LayoutInflater inflater;
    private ImageView imgMonAn;
    private TextView txtTenMonAn, txtNguyenLieu, txtCachCheBien;
    private ImageView play, yeuthich;
    private MonAn monAn;
    private MediaPlayer mediaPlayer;
    private MonAnController monAnController;
    private int yeuThich,current,length,firstVisit;
    private ArrayList<Integer> Songs = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chi_tiet_mon_an);

        firstVisit = 1;

        // Custom actionbar
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#8F0461")));
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar_chitietmonan);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        imgMonAn = (ImageView) findViewById(R.id.imgmonan);
        txtNguyenLieu = (TextView) findViewById(R.id.txtNguyenLieu);
        txtCachCheBien = (TextView) findViewById(R.id.txtCachCheBien);
        txtTenMonAn = (TextView) findViewById(R.id.idTenMonAn);
        yeuthich = (ImageView) findViewById(R.id.imgyeuthich);
        play = (ImageView) findViewById(R.id.imgMedia);
        play.setImageResource(R.drawable.icon_pause);
        // Nhan mon tu ben danh sach chi tiet gui den
        monAn = (MonAn) getIntent().getExtras().getSerializable("MONAN");
        yeuThich = monAn.getYeuThich();
        txtTenMonAn.setText(monAn.getTenMonAn());
        txtTenMonAn.setSingleLine(true);
        txtTenMonAn.setSelected(true);
        txtCachCheBien.setText(monAn.getCachCheBien());
        txtNguyenLieu.setText(monAn.getNguyenLieu());

        Picasso.with(getApplicationContext()).load(monAn.getHinhAnh()).into(imgMonAn);

        if (yeuThich == 1) {
            yeuthich.setImageResource(R.drawable.heart_red);
        } else {
            yeuthich.setImageResource(R.drawable.white_heart);
        }
        yeuthich.setOnClickListener(onclick_yeuthich);
        play.setOnClickListener(onclick_play);
        Songs.add(R.raw.song1);
        Songs.add(R.raw.song2);
        Songs.add(R.raw.song3);
        Songs.add(R.raw.song4);
        Songs.add(R.raw.song5);
        Songs.add(R.raw.angiday);
        Random random = new Random();
        current = random.nextInt(Songs.size());
        mediaPlayer = MediaPlayer.create(getApplicationContext(),Songs.get(current));
        mediaPlayer.start();

//        Animation zoom = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom);
//        zoom.reset();
//        txtNguyenLieu.clearAnimation();
//        txtNguyenLieu.startAnimation(zoom);
//        txtCachCheBien.clearAnimation();
//        txtCachCheBien.setAnimation(zoom);

    }


    @Override
    protected void onRestart() {
        super.onRestart();
        mediaPlayer = MediaPlayer.create(ChiTietMonAnActivity.this,Songs.get(current));
        mediaPlayer.start();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        mediaPlayer.pause();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mediaPlayer.pause();
        length = mediaPlayer.getCurrentPosition();
    }


//    @Override
//    protected void onResume() {
//        super.onResume();
//
//        if(firstVisit==1){
//            firstVisit=0;
//        }else {
//            mediaPlayer = MediaPlayer.create(ChiTietMonAnActivity.this, Songs.get(current));
//            mediaPlayer.seekTo(length);
//            mediaPlayer.start();
//        }
//    }

    private View.OnClickListener onclick_play = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(mediaPlayer.isPlaying()){
                mediaPlayer.pause();
                play.setImageResource(R.drawable.icon_start);
            }
            else {
                play.setImageResource(R.drawable.icon_pause);
                mediaPlayer.start();
            }
        }
    };


    private View.OnClickListener onclick_yeuthich = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //listMonAnYeuThich = new ArrayList<>();
            monAnController = new MonAnController(v.getContext());
            if (yeuThich == 0) {
                yeuthich.setImageResource(R.drawable.heart_red);
                SuperActivityToast.create(ChiTietMonAnActivity.this, new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText("Đã thêm vào danh sách yêu thích")
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(PaletteUtils.getSolidColor(PaletteUtils.MATERIAL_PURPLE))
                        .setAnimations(Style.ANIMATIONS_POP).show();
                yeuThich = 1;
                monAn.setYeuThich(yeuThich);
                for (MonAn monan : monAnController.getMonAnMienBac()) {
                    if (monAn.getTenMonAn().equals(monan.getTenMonAn())) {
                        monAnController.updateMonAnMienBac(monAn);
                        break;
                    }
                }
                for (MonAn monan : monAnController.getMonAnMienNam()) {
                    if (monAn.getTenMonAn().equals(monan.getTenMonAn())) {
                        monAnController.updateMonAnMienNam(monAn);
                        break;
                    }
                }
                for (MonAn monan : monAnController.getMonAnMienTrung()) {
                    if (monAn.getTenMonAn().equals(monan.getTenMonAn())) {
                        monAnController.updateMonAnMienTrung(monAn);
                        break;
                    }
                }
                for (MonAn monan : monAnController.getMonAnMienTay()) {
                    if (monAn.getTenMonAn().equals(monan.getTenMonAn())) {
                        monAnController.updateMonAnMienTay(monAn);
                        break;
                    }
                }


            } else {
                yeuthich.setImageResource(R.drawable.white_heart);
                SuperActivityToast.create(ChiTietMonAnActivity.this, new Style(), Style.TYPE_BUTTON)
                        .setProgressBarColor(Color.WHITE)
                        .setText("Đã loại khỏi danh sách yêu thích")
                        .setDuration(Style.DURATION_LONG)
                        .setFrame(Style.FRAME_LOLLIPOP)
                        .setColor(PaletteUtils.getSolidColor(PaletteUtils.MATERIAL_PURPLE))
                        .setAnimations(Style.ANIMATIONS_POP).show();
                yeuThich = 0;
                monAn.setYeuThich(yeuThich);
                //monAnController = new MonAnController(v.getContext());
                for (MonAn monan : monAnController.getMonAnMienBac()) {
                    if (monAn.getTenMonAn().equals(monan.getTenMonAn())) {
                        monAnController.updateMonAnMienBac(monAn);
                        break;
                    }
                }
                for (MonAn monan : monAnController.getMonAnMienNam()) {
                    if (monAn.getTenMonAn().equals(monan.getTenMonAn())) {
                        monAnController.updateMonAnMienNam(monAn);
                        break;
                    }
                }
                for (MonAn monan : monAnController.getMonAnMienTrung()) {
                    if (monAn.getTenMonAn().equals(monan.getTenMonAn())) {
                        monAnController.updateMonAnMienTrung(monAn);
                        break;
                    }
                }
                for (MonAn monan : monAnController.getMonAnMienTay()) {
                    if (monAn.getTenMonAn().equals(monan.getTenMonAn())) {
                        monAnController.updateMonAnMienTay(monAn);
                        break;
                    }
                }
            }
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.chitietmonan_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.hom_nay_an_gi:
                Intent homnayangi = new Intent(ChiTietMonAnActivity.this,HomNayAnGiActivity.class);
                startActivity(homnayangi);
                break;
            case R.id.mon_an_yeu_thich:
                Intent monanyeuthich = new Intent(ChiTietMonAnActivity.this,MonAnYeuThichActivity.class);
                startActivity(monanyeuthich);
                break;
            case R.id.meo_vat:
                Intent meovat = new Intent(ChiTietMonAnActivity.this,MeoVatActivity.class);
                startActivity(meovat);
                break;

        }
        return super.onOptionsItemSelected(item);
    }
}
