package com.example.workspace.nauan.fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.workspace.nauan.ChiTietMonAnActivity;
import com.example.workspace.nauan.MainActivity;
import com.example.workspace.nauan.R;
import com.example.workspace.nauan.RecyclerItemClickListener;
import com.example.workspace.nauan.adapter.MonAnAdapter;
import com.example.workspace.nauan.controller.MonAnController;
import com.example.workspace.nauan.model.MonAn;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_MienTrung extends Fragment {

    private RecyclerView recyclerViewMTrung;
    private GridLayoutManager grdManager;
    private MonAnAdapter monAnAdapter;
    private ArrayList<MonAn> dataSearch;
    private MaterialSearchView searchView;
    private Toolbar toolbar;
    private Context myContext;
    private MonAnController monAnController ;
    private MainActivity mainActivity = (MainActivity) getActivity();

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        myContext = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        monAnController = new MonAnController(myContext);
        View view =  inflater.inflate(R.layout.fragment_fragment__mien_trung, container, false);
        recyclerViewMTrung = (RecyclerView) view.findViewById(R.id.recyclerviewMTrung);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("");
        ((MainActivity)getActivity()).setToolbar(toolbar);
        // Tim
        searchView = (MaterialSearchView) getActivity().findViewById(R.id.search_view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        monAnAdapter = new MonAnAdapter(monAnController.getMonAnMienTrung());
        // set hieu ung
        AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(monAnAdapter);
        recyclerViewMTrung.setAdapter(new ScaleInAnimationAdapter(monAnAdapter));
        //recyclerViewMTrung.setAdapter(monAnAdapter);
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        grdManager = new GridLayoutManager(getContext(), 2);
        recyclerViewMTrung.setLayoutManager(grdManager);
        setHasOptionsMenu(true);
        //searchView.setVoiceSearch(true);
        recyclerViewMTrung.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                MonAn monAn = new MonAn();
                Intent chitiet = new Intent(getActivity(), ChiTietMonAnActivity.class);
                monAn = monAnController.getMonAnMienTrung().get(position);
                chitiet.putExtra("MONAN",monAn);
                startActivity(chitiet);
            }
        }));

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_search,menu);
        MenuItem item = menu.findItem(R.id.action_search); // Tìm đến controll searchview
        searchView.setMenuItem(item);
        searchView.setOnQueryTextListener(onTextChanged); // Bắt sự kiện khi clcik vào icon searchview

    }

    private MaterialSearchView.OnQueryTextListener onTextChanged = new MaterialSearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            dataSearch = new ArrayList<>();

            ArrayList<MonAn> listData = monAnController.getMonAnMienTrung();

            for (int i = 0; i < listData.size(); i++){
                if(listData.get(i).getTenMonAn().toLowerCase().contains(newText.toLowerCase())){
                    dataSearch.add(new MonAn(listData.get(i).getTenMonAn(), listData.get(i).getHinhAnh()));
                }
            }
            monAnAdapter = new MonAnAdapter(dataSearch);
            recyclerViewMTrung.setAdapter(monAnAdapter);
            monAnAdapter.notifyDataSetChanged();
            return false;
        }
    };


}
