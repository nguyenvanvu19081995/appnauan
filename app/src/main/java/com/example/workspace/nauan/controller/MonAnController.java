package com.example.workspace.nauan.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;

import com.example.workspace.nauan.model.MeoVat;
import com.example.workspace.nauan.model.MonAn;

import java.util.ArrayList;

/**
 * Created by WorkSpace on 11/24/2016.
 */

public class MonAnController extends SQLiteDataController {
    public MonAnController(Context con) {
        super(con);
    }

    public ArrayList<MeoVat> getMeoVat() {
        ArrayList<MeoVat> lsData = new ArrayList<>();
        try {
            //Bước 1: Mở kết nối DB
            openDataBase();
            //Bước 2: Truy vấn
            Cursor cs = database.rawQuery(
                    "select id,name,chitiet,hinhanh " +
                            "from MeoVat", null);
            while (cs.moveToNext()) {
                //Trỏ đến từng dòng
                int id = cs.getInt(0);
                String name = cs.getString(1);
                String chitiet = cs.getString(2);
                String hinhanh = cs.getString(3);


                MeoVat meovat = new MeoVat(id,name,chitiet, hinhanh);
                //Add vào danh sách
                lsData.add(meovat);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            //Bước 3: Đóng kết nối: Tại vì là nếu như bước 1,2 có lỗi
            //thì tôi vẫn phải đảm bảo DB được đóng lại
            close();
        }
        return lsData;
    }

    public ArrayList<MonAn> getMonAnMienBac() {
        ArrayList<MonAn> lsData = new ArrayList<>();
        try {
            //Bước 1: Mở kết nối DB
            openDataBase();
            //Bước 2: Truy vấn
            Cursor cs = database.rawQuery(
                    "select id,tenmonan,hinhanh,nguyenlieu,cachchebien,yeuthich " +
                            "from MonAnMienBac", null);
            while (cs.moveToNext()) {
                //Trỏ đến từng dòng
                int id = cs.getInt(0);
                String name = cs.getString(1);
                String hinhanh = cs.getString(2);
                String nguyenlieu = cs.getString(3);
                String cachchebien = cs.getString(4);
                int yeuthich = cs.getInt(5);

                MonAn monan = new MonAn(id, name, hinhanh, nguyenlieu, cachchebien, yeuthich);
                //Add vào danh sách
                lsData.add(monan);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            //Bước 3: Đóng kết nối: Tại vì là nếu như bước 1,2 có lỗi
            //thì tôi vẫn phải đảm bảo DB được đóng lại
            close();
        }
        return lsData;
    }

    public ArrayList<MonAn> getMonAnMienTrung() {
        ArrayList<MonAn> lsData = new ArrayList<>();
        try {
            //Bước 1: Mở kết nối DB
            openDataBase();
            //Bước 2: Truy vấn
            Cursor cs = database.rawQuery(
                    "select id,tenmonan,hinhanh,nguyenlieu,cachchebien,yeuthich " +
                            "from MonAnMienTrung", null);
            while (cs.moveToNext()) {
                //Trỏ đến từng dòng
                int id = cs.getInt(0);
                String name = cs.getString(1);
                String hinhanh = cs.getString(2);
                String nguyenlieu = cs.getString(3);
                String cachchebien = cs.getString(4);
                int yeuthich = cs.getInt(5);

                MonAn monan = new MonAn(id, name, hinhanh, nguyenlieu, cachchebien, yeuthich);
                //Add vào danh sách
                lsData.add(monan);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            //Bước 3: Đóng kết nối: Tại vì là nếu như bước 1,2 có lỗi
            //thì tôi vẫn phải đảm bảo DB được đóng lại
            close();
        }
        return lsData;
    }

    public ArrayList<MonAn> getMonAnMienNam() {
        ArrayList<MonAn> lsData = new ArrayList<>();
        try {
            //Bước 1: Mở kết nối DB
            openDataBase();
            //Bước 2: Truy vấn
            Cursor cs = database.rawQuery(
                    "select id,tenmonan,hinhanh,nguyenlieu,cachchebien,yeuthich " +
                            "from MonAnMienNam", null);
            while (cs.moveToNext()) {
                //Trỏ đến từng dòng
                int id = cs.getInt(0);
                String name = cs.getString(1);
                String hinhanh = cs.getString(2);
                String nguyenlieu = cs.getString(3);
                String cachchebien = cs.getString(4);
                int yeuthich = cs.getInt(5);

                MonAn monan = new MonAn(id, name, hinhanh, nguyenlieu, cachchebien, yeuthich);
                //Add vào danh sách
                lsData.add(monan);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            //Bước 3: Đóng kết nối: Tại vì là nếu như bước 1,2 có lỗi
            //thì tôi vẫn phải đảm bảo DB được đóng lại
            close();
        }
        return lsData;
    }

    public ArrayList<MonAn> getMonAnMienTay() {
        ArrayList<MonAn> lsData = new ArrayList<>();
        try {
            //Bước 1: Mở kết nối DB
            openDataBase();
            //Bước 2: Truy vấn
            Cursor cs = database.rawQuery(
                    "select id,tenmonan,hinhanh,nguyenlieu,cachchebien,yeuthich " +
                            "from MonAnMienTay", null);
            while (cs.moveToNext()) {
                //Trỏ đến từng dòng
                int id = cs.getInt(0);
                String name = cs.getString(1);
                String hinhanh = cs.getString(2);
                String nguyenlieu = cs.getString(3);
                String cachchebien = cs.getString(4);
                int yeuthich = cs.getInt(5);

                MonAn monan = new MonAn(id, name, hinhanh, nguyenlieu, cachchebien, yeuthich);
                //Add vào danh sách
                lsData.add(monan);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            //Bước 3: Đóng kết nối: Tại vì là nếu như bước 1,2 có lỗi
            //thì tôi vẫn phải đảm bảo DB được đóng lại
            close();
        }
        return lsData;
    }

    public ArrayList<MonAn> getMonAnYeuThich() {
        ArrayList<MonAn> lsData = new ArrayList<>();
        try {
            //Bước 1: Mở kết nối DB
            openDataBase();
            //Bước 2: Truy vấn
            Cursor cs = database.rawQuery(
                    "select id,tenmonan,hinhanh,nguyenlieu,cachchebien,yeuthich " +
                            "from MonAnYeuThich", null);
            Log.d("getMonAnMienTay: ",cs.getCount()+"");
            while (cs.moveToNext()) {
                //Trỏ đến từng dòng
                int id = cs.getInt(0);
                String name = cs.getString(1);
                String hinhanh = cs.getString(2);
                String nguyenlieu = cs.getString(3);
                String cachchebien = cs.getString(4);
                int yeuthich = cs.getInt(5);

                MonAn monan = new MonAn(id, name, hinhanh, nguyenlieu, cachchebien, yeuthich);
                //Add vào danh sách
                lsData.add(monan);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            //Bước 3: Đóng kết nối: Tại vì là nếu như bước 1,2 có lỗi
            //thì tôi vẫn phải đảm bảo DB được đóng lại
            close();
        }
        return lsData;
    }

    public boolean updateMonAnMienBac(MonAn ma) {
        boolean rs = false;
        try {
            openDataBase();
            //Đây là một bản ghi
            ContentValues values = new ContentValues();
            values.put("ID",ma.getId());
            values.put("YeuThich", ma.getYeuThich());
            long id = database.update("MonAnMienBac", values, "ID=" + ma.getId(), null);
            //id chính là trường ID
            if (id >= 0) {
                //update thanh cong
                rs = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close();
        }
        return rs;
    }

    public boolean updateMonAnMienTrung(MonAn ma) {
        boolean rs = false;
        try {
            openDataBase();
            //Đây là một bản ghi
            ContentValues values = new ContentValues();
            values.put("ID",ma.getId());
            values.put("YeuThich", ma.getYeuThich());
            long id = database.update("MonAnMienTrung", values, "ID=" + ma.getId(), null);
            //id chính là trường ID
            if (id >= 0) {
                //update thanh cong
                rs = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close();
        }
        return rs;
    }

    public boolean updateMonAnMienNam(MonAn ma) {
        boolean rs = false;
        try {
            openDataBase();
            //Đây là một bản ghi
            ContentValues values = new ContentValues();
            values.put("ID",ma.getId());
            values.put("YeuThich", ma.getYeuThich());
            long id = database.update("MonAnMienNam", values, "ID=" + ma.getId(), null);
            //id chính là trường ID
            if (id >= 0) {
                //update thanh cong
                rs = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close();
        }
        return rs;
    }

    public boolean updateMonAnMienTay(MonAn ma) {
        boolean rs = false;
        try {
            openDataBase();
            //Đây là một bản ghi
            ContentValues values = new ContentValues();
            values.put("ID",ma.getId());
            values.put("YeuThich", ma.getYeuThich());
            long id = database.update("MonAnMienTay", values, "ID=" + ma.getId(), null);
            //id chính là trường ID
            if (id >= 0) {
                //update thanh cong
                rs = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close();
        }
        return rs;
    }

    public boolean deleteMonAn(int id) {
        boolean result = false;
        try {
            openDataBase();
            int _id = database.delete("MonAnYeuThich", "ID=" + id, null);
            if (_id >= 0)
                result = true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close();
        }
        return result;
    }

    public boolean insertMonAnYeuThich(MonAn monAn) {
        boolean rs = false;
        try {
            openDataBase();
            //Đây là một bản ghi
            ContentValues values = new ContentValues();
            values.put("id", monAn.getId());
            values.put("tenmonan", monAn.getTenMonAn());
            values.put("hinhanh", monAn.getHinhAnh());
            values.put("nguyenlieu", monAn.getNguyenLieu());
            values.put("cachchebien", monAn.getCachCheBien());
            values.put("yeuthich", monAn.getYeuThich());

            long id = database.insert("MonAnYeuThich", null, values);
            //id chính là trường ID
            if (id >= 0) {
                //Insert thanh cong
                rs = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close();
        }
        return rs;
    }

}
