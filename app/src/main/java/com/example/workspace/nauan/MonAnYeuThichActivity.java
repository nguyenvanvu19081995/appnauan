package com.example.workspace.nauan;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.example.workspace.nauan.adapter.MonAnYeuThichAdapter;
import com.example.workspace.nauan.controller.MonAnController;
import com.example.workspace.nauan.model.MonAn;

import java.util.ArrayList;

public class MonAnYeuThichActivity extends AppCompatActivity {

    private static MonAnYeuThichActivity instance;
    private ArrayList<MonAn> listData;
    private ListView listView;
    private MonAnYeuThichAdapter adapter;
    private MonAnController monAnController ;

    public static MonAnYeuThichActivity getInstance() {
        return instance;
    }


    public static Context getContext() {
        return instance.getApplicationContext();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mon_an_yeu_thich);

        instance = this;

        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#8F0461")));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Món ăn yêu thích");

        monAnController = new MonAnController(getContext());

        listView = (ListView) findViewById(R.id.lvMonAnYeuThich);

        listData = getListMonAnYeuThich(monAnController);
        Log.d("sizeMonAnYeuThich: ",listData.size()+"");
        adapter = new MonAnYeuThichAdapter(listData,getLayoutInflater());
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(onItem_Click);
        listView.setOnItemLongClickListener(onDelete_Click);
    }

    private AdapterView.OnItemLongClickListener onDelete_Click = new AdapterView.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> adapterView, final View view, final int position, long l) {
            final MaterialDialog.Builder builder1 = new MaterialDialog.Builder(view.getContext());
            builder1.title("Xóa");
            builder1.content("Bạn có thật sự muốn xóa món ăn ra khỏi danh sách yêu thích không?");
            builder1.positiveText("OK");
            builder1.negativeText("Cancel");
            //builder1.show();
            MaterialDialog materialDialog = builder1.build();
            builder1.onPositive(new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                    MonAnController monAnController = new MonAnController(view.getContext());
                    MonAn monAn = listData.get(position);
                    monAn.setYeuThich(0);
                    for (MonAn monan : monAnController.getMonAnMienBac()) {
                        if (monAn.getTenMonAn().equals(monan.getTenMonAn())) {
                            monAnController.updateMonAnMienBac(monAn);
                            break;
                        }
                    }
                    for (MonAn monan : monAnController.getMonAnMienNam()) {
                        if (monAn.getTenMonAn().equals(monan.getTenMonAn())) {
                            monAnController.updateMonAnMienNam(monAn);
                            break;
                        }
                    }
                    for (MonAn monan : monAnController.getMonAnMienTrung()) {
                        if (monAn.getTenMonAn().equals(monan.getTenMonAn())) {
                            monAnController.updateMonAnMienTrung(monAn);
                            break;
                        }
                    }
                    for (MonAn monan : monAnController.getMonAnMienTay()) {
                        if (monAn.getTenMonAn().equals(monan.getTenMonAn())) {
                            monAnController.updateMonAnMienTay(monAn);
                            break;
                        }
                    }
                    listData.remove(position);
                    adapter.notifyDataSetChanged();
                    materialDialog.cancel();
                }
            });
            builder1.onNegative(new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                    materialDialog.dismiss();

                }
            });
            materialDialog.show();


            return true;
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_monanyeuthich,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.deleteAll:
                final MaterialDialog.Builder builder1 = new MaterialDialog.Builder(this);
                builder1.title("Xóa tất cả");
                builder1.content("Bạn có thật sự muốn xóa tất cả món ăn ra khỏi danh sách yêu thích không?");
                builder1.positiveText("OK");
                builder1.negativeText("Cancel");
                //builder1.show();
                final MaterialDialog materialDialog = builder1.build();
                builder1.onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        for (MonAn monan : monAnController.getMonAnMienBac()) {
                            if (monan.getYeuThich() == 1) {
                                monan.setYeuThich(0);
                                monAnController.updateMonAnMienBac(monan);
                            }
                        }
                        for (MonAn monan : monAnController.getMonAnMienNam()) {
                            if (monan.getYeuThich() == 1) {
                                monan.setYeuThich(0);
                                monAnController.updateMonAnMienBac(monan);
                            }
                        }
                        for (MonAn monan : monAnController.getMonAnMienBac()) {
                            if (monan.getYeuThich() == 1) {
                                monan.setYeuThich(0);
                                monAnController.updateMonAnMienBac(monan);
                            }
                        }
                        for (MonAn monan : monAnController.getMonAnMienNam()) {
                            if (monan.getYeuThich() == 1) {
                                monan.setYeuThich(0);
                                monAnController.updateMonAnMienBac(monan);
                            }
                        }
                        listData.clear();
                        adapter.notifyDataSetChanged();
                    }
                });
                builder1.onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        materialDialog.dismiss();
                    }
                });
                materialDialog.show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private AdapterView.OnItemClickListener onItem_Click = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            MonAn monAn = new MonAn();
            Intent chitiet = new Intent(MonAnYeuThichActivity.this, ChiTietMonAnActivity.class);
            monAn = adapter.getItem(position);
            chitiet.putExtra("MONAN",monAn);
            startActivityForResult(chitiet,100);
        }
    };

    public ArrayList<MonAn> getListMonAnYeuThich(MonAnController monAnController){
        ArrayList<MonAn> listMonAnYeuThich = new ArrayList<>();
        for (MonAn monan : monAnController.getMonAnMienBac()) {
            if(monan.getYeuThich() == 1){
                listMonAnYeuThich.add(monan);
            }
        }
        for (MonAn monan : monAnController.getMonAnMienNam()) {
            if(monan.getYeuThich() == 1){
                listMonAnYeuThich.add(monan);
            }
        }
        for (MonAn monan : monAnController.getMonAnMienTrung()) {
            if(monan.getYeuThich() == 1){
                listMonAnYeuThich.add(monan);
            }
        }
        for (MonAn monan : monAnController.getMonAnMienTay()) {
            if(monan.getYeuThich() == 1){
                listMonAnYeuThich.add(monan);
            }
        }
        return listMonAnYeuThich;

    }
}
