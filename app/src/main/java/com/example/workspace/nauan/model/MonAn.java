package com.example.workspace.nauan.model;

import java.io.Serializable;

/**
 * Created by WorkSpace on 11/24/2016.
 */

public class MonAn implements Serializable {
    private int id;
    private String tenMonAn;
    private String hinhAnh;
    private String nguyenLieu;
    private String cachCheBien;
    private int yeuThich;

    public MonAn() {

    }

    public MonAn(String tenMonAn, String hinhAnh) {
        this.tenMonAn = tenMonAn;
        this.hinhAnh = hinhAnh;
    }

    public MonAn(int id, String tenMonAn, String hinhAnh, String nguyenLieu, String cachCheBien, int yeuThich) {
        this.id = id;
        this.tenMonAn = tenMonAn;
        this.hinhAnh = hinhAnh;
        this.nguyenLieu = nguyenLieu;
        this.cachCheBien = cachCheBien;
        this.yeuThich = yeuThich;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTenMonAn() {
        return tenMonAn;
    }

    public void setTenMonAn(String tenMonAn) {
        this.tenMonAn = tenMonAn;
    }

    public String getHinhAnh() {
        return hinhAnh;
    }

    public void setHinhAnh(String hinhAnh) {
        this.hinhAnh = hinhAnh;
    }

    public String getNguyenLieu() {
        return nguyenLieu;
    }

    public void setNguyenLieu(String nguyenLieu) {
        this.nguyenLieu = nguyenLieu;
    }

    public String getCachCheBien() {
        return cachCheBien;
    }

    public void setCachCheBien(String cachCheBien) {
        this.cachCheBien = cachCheBien;
    }

    public int getYeuThich() {
        return yeuThich;
    }

    public void setYeuThich(int yeuThich) {
        this.yeuThich = yeuThich;
    }
}
