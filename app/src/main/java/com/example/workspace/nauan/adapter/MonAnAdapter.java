package com.example.workspace.nauan.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.workspace.nauan.R;
import com.example.workspace.nauan.model.MonAn;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by WorkSpace on 11/24/2016.
 */

public class MonAnAdapter extends RecyclerView.Adapter<MonAnAdapter.MonAnViewHolder> {
    private ArrayList<MonAn> lsData;
    private LayoutInflater inflater;
    private Context context;
    //int xacdinh=1;
    //private int lastPosition = -1;
    public MonAnAdapter(ArrayList<MonAn> lsData) {
        this.lsData = lsData;
    }

    @Override
    public MonAnViewHolder  onCreateViewHolder(ViewGroup parent, int viewType) {
        inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.monan_item, parent, false);
        MonAnViewHolder holder = new MonAnViewHolder(view);
        RecyclerView.LayoutParams pr = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        pr.setMargins(15,15,15,15);
        view.setLayoutParams(pr);
        return holder;
    }

    @Override
    public void onBindViewHolder(MonAnViewHolder holder, final int position) {

        holder.txtMonAn.setText(lsData.get(position).getTenMonAn());
        Picasso.with(inflater.getContext()).load(lsData.get(position).getHinhAnh()).into(holder.imgMonAn);
//        Toast.makeText(context, lsData.get(1).getTenMonAn(), Toast.LENGTH_SHORT).show();
//        Log.d("onBindViewHolder: ",lsData.get(1).getTenMonAn());
        holder.txtMonAn.setSelected(true);
        holder.txtMonAn.setSingleLine(true);
    }

    @Override
    public int getItemCount() {
        return lsData.size();
    }

    class MonAnViewHolder extends RecyclerView.ViewHolder {
        ImageView imgMonAn;
        TextView txtMonAn;
        public MonAnViewHolder(View itemView) {
            super(itemView);
            imgMonAn = (ImageView) itemView.findViewById(R.id.imgMonAn);
            txtMonAn = (TextView) itemView.findViewById(R.id.txtMonAn);
        }
    }


}
